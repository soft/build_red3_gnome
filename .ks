timezone Europe/Moscow
auth --useshadow --enablemd5
selinux --disabled
firewall --enabled
firstboot --enabled
part / --size 8692

services --enabled=acpid,alsa,atd,avahi-daemon,irqbalance,mandi,dbus,netfs,partmon,resolvconf,rpcbind,rsyslog,sound,udev-post,mandrake_everytime,crond,cups,smb
services --disabled=sshd,pptp,pppoe,ntpd,iptables,ip6tables,shorewall,nfs-server,mysqld,abrtd,mysql,postfix,network,cpupower,winbind,packagekit-offline-update,openvpn

repo --name=Main           --baseurl=http://abf.rosalinux.ru/downloads/red3/repository/i586/main/release
#repo --name=Main-Updates   --baseurl=http://abf.rosalinux.ru/downloads/red3/repository/i586/main/updates
repo --name=Non-Free       --baseurl=http://abf.rosalinux.ru/downloads/red3/repository/i586/non-free/release
#repo --name=Non-Free-Up    --baseurl=http://abf.rosalinux.ru/downloads/red3/repository/i586/non-free/updates

repo --name=Contrib        --baseurl=http://abf.rosalinux.ru/downloads/red3/repository/i586/contrib/release
#repo --name=Contrib-Up     --baseurl=http://abf.rosalinux.ru/downloads/red3/repository/i586/contrib/updates

repo --name=Restricted     --baseurl=http://abf.rosalinux.ru/downloads/red3/repository/i586/restricted/release
#repo --name=Restricted-Up  --baseurl=http://abf.rosalinux.ru/downloads/red3/repository/i586/restricted/updates

# Custom
%include .///i586repo.lst

%packages
%include .///gnome.lst
%include .///i586libs.lst
%end

%post

# keyboard layout switcher

cat >> /etc/X11/xsetup.d/40finish-install.xsetup << FOE
if [ -f /etc/sysconfig/keyboard ]; then
  . /etc/sysconfig/keyboard

  # Set system keyboard layouts as default for GNOME 3.8

  echo \$XkbLayout | grep "," > /dev/null

  if [ \$? -eq 0 ]; then
      L1=\$(echo \$XkbLayout | awk -F, '{ print \$1 }')
      L2=\$(echo \$XkbLayout | awk -F, '{ print \$2 }')
      cat > /usr/share/glib-2.0/schemas/org.gnome.desktop.input-sources.gschema.override << EOF
[org.gnome.desktop.input-sources]
sources=[('xkb', '\$L1'), ('xkb', '\$L2')]
EOF

      # Keyboard switcher.

      if [ "\$GRP_TOGGLE" != "" ]; then
   	    GNOME_TOGGLE="grp:\$GRP_TOGGLE"

            cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.input-sources.gschema.override <<EOF
xkb-options=['\$GNOME_TOGGLE']
EOF
      fi

  fi

  /usr/bin/glib-compile-schemas /usr/share/glib-2.0/schemas/ > /dev/null 2>&1

fi

FOE

# fonts
cat > /usr/share/glib-2.0/schemas/org.gnome.settings-daemon.plugins.xsettings.gschema.override <<FOE
[org.gnome.settings-daemon.plugins.xsettings]
antialiasing='rgba'
hinting='slight'
FOE


cat > /usr/share/glib-2.0/schemas/org.gnome.desktop.interface.gschema.override <<FOE
[org.gnome.desktop.interface]
monospace-font-name='Droid Sans Mono 10'
icon-theme='rosa'
gtk-theme='rosa-elementary'
FOE

cat > /usr/share/glib-2.0/schemas/org.gnome.desktop.wm.preferences.gschema.override <<FOE
[org.gnome.desktop.wm.preferences]
theme='rosa-elementary'
FOE

cat > /usr/share/glib-2.0/schemas/org.gnome.shell.gschema.override <<FOE
[org.gnome.shell]
enabled-extensions=['user-theme@gnome-shell-extensions.gcampax.github.com', 'dash-to-dock@micxgx.gmail.com', 'notifications-alert-on-user-menu@hackedbellini.gmail.com', 'weather-extension@xeked.com', 'alternative-status-menu@gnome-shell-extensions.gcampax.github.com', 'SkypeNotification@chrisss404.gmail.com', 'ROSA_Hotkeys@uxteam.rosalab.ru']
favorite-apps=['firefox.desktop', 'chromium-browser.desktop', 'evolution.desktop', 'empathy.desktop', 'shotwell.desktop', 'libreoffice-writer.desktop', 'nautilus.desktop', 'rpmdrake.desktop', 'rosa-draklive-install.desktop']

[org.gnome.shell.overrides]
button-layout=':minimize,maximize,close'
FOE

# we will remove lock override in remove-live-user.sh
# disable screensaver locking
cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.screensaver.gschema.override <<FOE
[org.gnome.desktop.screensaver]
lock-enabled=false
FOE

# set nautilus icon-view default-zool-level to large
cat > /usr/share/glib-2.0/schemas/org.gnome.nautilus.gschema.override <<FOE
[org.gnome.nautilus.icon-view]
default-zoom-level='large'
captions=['none', 'none', 'date_modified']

[org.gnome.nautilus.preferences]
sort-directories-first=true
FOE

# and hide the lock screen option
cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.lockdown.gschema.override <<FOE
[org.gnome.desktop.lockdown]
disable-lock-screen=true
FOE

cat > /usr/share/glib-2.0/schemas/org.gnome.shell.extensions.user-theme.gschema.override <<FOE
[org.gnome.shell.extensions.user-theme]
name='rosa-elementary'
FOE

cat > /usr/share/glib-2.0/schemas/org.gnome.desktop.background.gschema.override <<FOE
[org.gnome.desktop.background]
picture-uri='file:///usr/share/mdk/backgrounds/Mandriva-Rosa.xml'
FOE

cat > /usr/share/glib-2.0/schemas/org.gnome.Terminal.gschema.override <<FOE
[org.gnome.Terminal.Legacy.Settings]
menu-accelerator-enabled=false
FOE

/usr/bin/glib-compile-schemas /usr/share/glib-2.0/schemas/ > /dev/null 2>&1

# generate session files
/usr/sbin/chksession -g

# use rosa-install script to install desktop from menu
sed -i 's!Exec=/usr/bin/draklive-install-lock-storage!Exec=/usr/bin/rosa-install!g' \
	/usr/share/applications/rosa-draklive-install.desktop

# delete bad gnome-disks icon
find /usr/share/icons/rosa -name "gnome-disks*" -delete

# Force to make icon cache

for i in `find /usr/share/icons/ -name "index.theme"`; do
    /usr/bin/gtk-update-icon-cache --force --quiet `dirname $i`;
done

rm -f /usr/share/applications/mimeapps.list
rm -f /etc/X11/dm/Sessions/03*desktop

## GDM autologin
cat > /etc/X11/gdm/custom.conf << FOE
# GDM configuration storage

[daemon]
AutomaticLoginEnable=True
AutomaticLogin=live

[security]

[xdmcp]

[greeter]

[chooser]

[debug]

FOE

# drop systemd bash completion. Maybe it GNOME requires
# recent systemd and send some bad paremeters somewhere.
# In this case gdm fails to login user sometimes.
# Deep testing needed
rm -f /etc/bash_completion.d/systemd

# drop plumouth from conflicts
sed -i '/Conflicts/ s! plymouth-quit.service!!g' /lib/systemd/system/gdm.service

# Disable sendmail
/sbin/chkconfig --del sendmail

##Enable DM - use you DM service name!
##systemctl enable gdm.service
systemctl disable lightdm.service
## LIGHTDM enable xsetup.d support
##sed -e 's/#display-setup-script/display-setup-script/' -i /etc/lightdm/lightdm.conf

## set up lightdm autologin
#sed -i 's/^#autologin-user=.*/autologin-user=live/' /etc/lightdm/lightdm.conf
#sed -i 's/^#autologin-session=.*/autologin-session=02GNOME/' /etc/lightdm/lightdm.conf
#sed -i 's/^#user-session=.*/user-session=02GNOME/' /etc/lightdm/lightdm.conf
#sed -i 's/^#autologin-user-timeout=.*/autologin-user-timeout=0/' /etc/lightdm/lightdm.conf
##sed -i 's/^#show-language-selector=.*/show-language-selector=true/' /etc/lightdm/lightdm-gtk-greeter.conf
##### make a symlink so lightdm autologin works ok
##ln -s /usr/share/xsessions/02GNOME.desktop /usr/share/xsessions/gnome.desktop
#####
# delete EE splashes
rm -fv /usr/share/libDrakX/advert/*/ROSAOne-advert-[5-9].png

# delete GNOME PackageKit icon
rm -f /usr/share/applications/gpk-application.desktop

rm -f /etc/skel/.fonts.conf

#sed -i 's!MandrivaLinux!ROSALinux!g' /etc/lsb-release
#sed -i 's!Mandriva!ROSA Desktop!g' /etc/lsb-release
#sed -i 's!2011.0!2012.1!g' /etc/lsb-release                                                                                                                                                            

#We use GNOME Edition stuff (like GE)
sed -i 's/EE/GE/' /etc/product.id
sed -i 's/EE/GE/' /etc/product.id
                                                      
echo "###################################### Make initrd symlink >> "
echo ""

/usr/sbin/update-alternatives --set mkinitrd /usr/sbin/mkinitrd-dracut
rm -rf /boot/initrd-*

# adding life user
/usr/sbin/adduser -c "ROSA Live User" live
/usr/bin/passwd -d live
/bin/mkdir -p /home/live
/bin/cp -rfT /etc/skel /home/live/
/bin/chown -R live:live /home/live

# set up icon
mkdir -p /var/lib/AccountsService/users/
mkdir -p /var/lib/AccountsService/icons/

# don't run gnome-initial-setup
mkdir ~live/.config
touch ~live/.config/gnome-initial-setup-done

cat > /var/lib/AccountsService/users/live << FOE
[User]
Language=
XSession=
Icon=/var/lib/AccountsService/icons/live
FOE

# ldetect stuff
/usr/sbin/update-ldetect-lst
/usr/sbin/update-pciids

# setting up network manager by default
# don't forget to change it

#pushd /etc/sysconfig/network-scripts
#for iface in eth0 wlan0; do
#	cat > ifcfg-$iface << EOF
#DEVICE=$iface
#ONBOOT=yes
#NM_CONTROLLED=yes
#EOF
#done
#popd

systemctl enable NetworkManager.service
systemctl enable getty@.service

###temp workarond, remove when sure###
#systemctl disable cups.service

##NM need USE_ONLY=yes
#echo 'USE_NM_ONLY=yes' >> /etc/sysconfig/network

# Enable DM - use you DM service name!	
systemctl enable gdm.service

# Enable Samba
#systemctl enable smb.service

# default background
pushd /usr/share/mdk/backgrounds/
ln -s rosa-background.jpg default.jpg 
popd

###chkconfig###                                                                                                                                                                                                    
/sbin/chkconfig --add checkflashboot                                                                                                                                                                               
echo "RUN CHKSESSION"
chksession -g

#
# DKMS
#

echo
echo
echo Rebuilding DKMS drivers
echo
echo

export PATH=/bin:/sbin:/usr/bin:/usr/sbin

#build arch import for vboxadditions dkms + flash workaround###

export BUILD_TARGET_ARCH=x86
XXX=`file /bin/rpm |grep -c x86-64`
if [ "$XXX" = "1" ]; then
export BUILD_TARGET_ARCH=amd64
fi

echo " ###DKMS BUILD### "
kernel_ver=`ls /boot | /bin/grep vmlinuz | /bin/sed 's/vmlinuz-//' |head -1`
for module in vboxadditions broadcom-wl; do
module_version=`rpm --qf '%{VERSION}\n' -q dkms-$module`
module_release=`rpm --qf '%{RELEASE}\n' -q dkms-$module`
su --session-command="/usr/sbin/dkms -k $kernel_ver -a i586 --rpm_safe_upgrade add -m $module -v $module_version-$module_release" root
su --session-command="/usr/sbin/dkms -k $kernel_ver -a i586 --rpm_safe_upgrade build -m $module -v $module_version-$module_release" root
su --session-command="/usr/sbin/dkms -k $kernel_ver -a i586 --rpm_safe_upgrade install -m $module -v $module_version-$module_release --force" root
done

echo "END OF IT".

#
# Sysfs must be mounted for dracut to work!
#
mount -t sysfs /sys /sys
ln -s /usr/share/plymouth/themes/Mandriva-Rosa/rosa.png /usr/share/plymouth/themes/Mandriva-Rosa/welcome.png
pushd /lib/modules/
KERNEL=$(echo *)
popd
echo
echo Generating kernel. System kernel is `uname -r`, installed kernels are:
rpm -qa kernel-*
echo Detected kernel version: $KERNEL

sed -i 's/omit\_drivers/\#omit\_drivers/g' /etc/draklive-install.d/isobuild/60-dracut-isobuild.conf
/usr/sbin/dracut /boot/initramfs-$KERNEL.img $KERNEL --force --confdir /etc/draklive-install.d/isobuild/ -v
/usr/sbin/dracut /boot/initrd-$KERNEL.img $KERNEL --force --confdir /etc/draklive-install.d/isobuild/ -v
mkdir -p /run/initramfs/live/isolinux/
ln -s /boot/initramfs-$KERNEL.img /run/initramfs/live/isolinux/initrd0.img
#cp -f /boot/initramfs-$KERNEL.img /run/initramfs/live/isolinux/initrd0.img
#ls -l /boot/

#hack for nscd loop error
while (ps -e | grep nscd)
do
  killall -s 9 nscd
done

####
####
#### Temp remove Elementary Theme. Drop after final gnome-shell-user-theme-elementary
####
####
rm -f /usr/share/themes/Elementary/gnome-shell/*

echo ""
echo "###################################### Build ISO >> "
echo ""
%end

%post --nochroot
#hack to try to stop umount probs
while (.///lsof /dev/loop* | grep -v "$0" | grep "$INSTALL_ROOT")
do
 sleep 5s
done

   cp -rfT 	.///extraconfig/etc $INSTALL_ROOT/etc/
    cp -rfT 	.///extraconfig/usr $INSTALL_ROOT/usr/
    cp -rfT 	.///extraconfig/var $INSTALL_ROOT/var/
    cp -rfT 	.///extraconfig/var $INSTALL_ROOT/root/
    cp -rfT     .///extraconfig/etc/skel $INSTALL_ROOT/home/live/
    cp -rfT	.///extraconfig/usr/share/pixmaps/rosa.png $INSTALL_ROOT/var/lib/AccountsService/icons/live
    chown -R 500:500 $INSTALL_ROOT/home/live/
    chmod -R 0777 $INSTALL_ROOT/home/live/.local
    chmod -R 0777 $INSTALL_ROOT/home/live/.kde4
    cp -rfT     .///welcome.jpg $INSTALL_ROOT/splash.jpg
    cp -rfT     .///welcome.jpg $INSTALL_ROOT/welcome.jpg
    cp -rfT     .///welcome.jpg $INSTALL_ROOT/splash.jpg

#workaround for flash-plugin
### temporary disable it
cp -rfT /etc/resolv.conf $INSTALL_ROOT/etc/resolv.conf
/usr/sbin/urpmi.removemedia -a
/usr/sbin/urpmi.addmedia --distrib  --all-media http://abf.rosalinux.ru/downloads/rosa2012.1/repository/i586/
/usr/sbin/urpmi --root $INSTALL_ROOT flash-player-plugin
echo > $INSTALL_ROOT/etc/resolv.conf
#end of it

# install repos
cp -rfT /etc/resolv.conf $INSTALL_ROOT/etc/resolv.conf
/usr/sbin/urpmi.removemedia -a
/usr/sbin/chroot $INSTALL_ROOT /usr/sbin/urpmi.addmedia --distrib http://mirror.rosalab.ru/rosa/rosa2012.1/repository/i586/
/usr/sbin/chroot $INSTALL_ROOT urpmi.update -a

echo > $INSTALL_ROOT/etc/resolv.conf
#end of it

#ssh key don't need
    rm -f $INSTALL_ROOT/etc/ssh/*key*

    cp -rfT     .///.counter $INSTALL_ROOT/etc/isonumber 
    mkdir -p $LIVE_ROOT/isolinux/
    cp .///extraconfig/memdisk $LIVE_ROOT/isolinux/
    cp .///extraconfig/sgb.iso $LIVE_ROOT/isolinux/

    cp -f 		.///root/GPL $LIVE_ROOT/
    rpm --root $INSTALL_ROOT -qa | sort > $LIVE_ROOT/rpm.lst
    ./total_sum_counter.pl -r 640 -h 10 -w $INSTALL_ROOT/ -o $INSTALL_ROOT/etc/minsysreqs

%end
